var gulp = require('gulp');
var sass = require('gulp-sass');
var uglify = require('gulp-uglify');
var gulpIf = require('gulp-if');
var concat = require('gulp-concat');
var refresh = require('gulp-refresh');

var config = {
    fontawesomeDir: './bower_components/components-font-awesome',
    assetsDir: './assets',
    sassPath: './sass'
};

gulp.task('css', function() {
    return gulp.src(['sass/styles.scss'])
    .pipe(sass({
        includePaths: [config.fontawesomeDir + '/scss'],
        outputStyle: 'compressed'
    }))
    .pipe(refresh())
    .pipe(gulp.dest(config.assetsDir + '/css'));
});

gulp.task('jquery', function() {
  	gulp.src(['./bower_components/jquery/dist/jquery.min.js'])
	    .pipe(concat('jquery.min.js'))
	    .pipe(uglify())
	    .pipe(gulp.dest(config.assetsDir + '/js'))
        .pipe(refresh());
});

gulp.task('fonts1', function() {
    return gulp.src(config.bootstrapDir + '/assets/fonts/**/*')
    .pipe(gulp.dest(config.assetsDir + '/fonts'))
    .pipe(refresh());
});

gulp.task('fonts2', function() {
    return gulp.src(config.fontawesomeDir + '/fonts/*')
    .pipe(gulp.dest(config.assetsDir + '/fonts'))
    .pipe(refresh());
});

gulp.task('default', ['css', 'jquery', 'fonts1', 'fonts2']);

