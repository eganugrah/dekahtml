$(document).ready(function() {
	// resizeDevice();
});
$(document).scroll(function() {
	var y = $(this).scrollTop();
	if (y > 20) {
		$('.header').addClass('sticky');
	} else {
		$('.header').removeClass('sticky');
	}
});

function resizeDevice() {
	var winWidth = $(window).width();
	if (winWidth < 1200) {
		var logoSpace = $('.logo-space');
		$(logoSpace).remove().clone().prependTo('#mainMenu > ul');
	}
	if (winWidth < 768) {
		$('.copyright').parent().remove().clone().appendTo('.footer');
	}
}

$(window).on('load', function () {
	$('body').fadeIn('slow');
	resizeDevice();
});

$(function() {
	$('.topnav li:not(.has-child) a:not(.icon-burger)').click(function() {
		var target = $(this).attr('data-target');
	    $('html, body').animate({
	        scrollTop: $(target).offset().top-80
	    }, 1000);
		$('#mainMenu').removeClass('responsive');
	});
	$(".topnav li.has-child a").click(function() {
		var currentMenu = $(this).next('.child-menu');
		$('.topnav li.has-child ul.child-menu').not(currentMenu).removeClass('show');
		$(currentMenu).toggleClass('show');
	});
});
$(document).mouseup(function(e) {
	var navChildMenu = $('.topnav li.has-child a');
    if (!navChildMenu.is(e.target) && navChildMenu.has(e.target).length === 0) {
        $('ul.child-menu').removeClass('show');
    }
});
function navToggle() {
    var idMenu = $('#mainMenu');
    $(idMenu).toggleClass('responsive');
}

		